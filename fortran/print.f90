program main
  double precision, dimension(3) :: x = (/1d0, 3d0, 5d0/)
  print '("x = ", f15.8)', 2.0d0
  print *, 1d0
  print *, 1000d0
  print '(f)', 1000d0
  print '(f)', 100d0
  print '(f10.8,",", f10.8,",", f10.8)', x
end program
