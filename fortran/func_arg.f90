module functions_module
  implicit none
contains
  double precision function myfunc(x)
    double precision, intent(in) :: x
    myfunc = sin(x) ** 2
  end function
end module

program main
  use functions_module
  implicit none
  print *, sekibun(myfunc, 0d0, 3.1415926d0)
contains
  double precision function sekibun(f, a, b)
    interface
      double precision function f(x)
        double precision, intent(in) :: x
      end function
    end interface
    double precision, intent(in) :: a, b
    double precision x1, step, x2
    integer i
    integer, parameter :: n = 100
    step = (b - a) / n
    sekibun = 0d0
    x1 = a
    do i = 1, n
      x2 = a + step*i
      sekibun = sekibun + (f(x1)+f(x2))*(x2-x1)/2d0
      x1 = x2
    end do
  end function
end program 
