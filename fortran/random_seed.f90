program random_seed_p
  implicit none
  integer,parameter :: n = 5
  real x
  integer i, seedsize
  integer,allocatable,dimension(:) :: seed

  call random_seed(size=seedsize)
  allocate(seed(seedsize))
  call random_seed(get=seed)

  print *, "Size of seed array is", seedsize

  print *, "1st try ..."
  do i=1, n
    call random_number(x)
    print *, i, x
  end do

  call random_seed(put=seed)
  
  print *, "2nd try ..."
  do i=1, n
    call random_number(x)
    print *, i, x
  end do
end program
