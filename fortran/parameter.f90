program parameter_attr
        implicit none
        real r
        real :: p=3.14 ! parameter 属性を指定しない

        p = 1.0 ! 意図しない代入

        print *, "Enter radius:"
        read *, r
        print *, r, "x", r, "x 3.14 =", r*r*p
end program parameter_attr
