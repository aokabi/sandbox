program main
  real,dimension(3) :: x,y
  real,dimension(3) :: z = (/3,2,1/)
  x(1) = 1
  x(2) = 2
  x(3) = 3

  y(1) = 1
  y(2) = 2
  y(3) = 4

  print *, x+y
  print *, z
  print *, x*y
end program
