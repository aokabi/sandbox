program main
  double precision :: c = 3.0 * 1e8
  double precision :: m = 9.109 * 1e-31
  double precision :: K = 1.602*1e6*1e-19

  print '(e)', c * sqrt(1 - (m*c**2)/(K + m*c**2)) 
end program
